﻿using System.ComponentModel.DataAnnotations;

namespace Portal.Models
{
    public class OrderCreateVm
    {
        [Required]
        public double Price { get; set; }        
    }
}