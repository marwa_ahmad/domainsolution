﻿using System.Collections.Generic;


namespace Portal.Models
{
    public class CustomerDataGetVm : CustomerGetVm
    {
        public List<OrderGetVm> Orders { get; set; }
    }
}