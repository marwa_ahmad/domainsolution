﻿using System;

namespace Portal.Models
{
    public class OrderGetVm
    {
        public string Id { get; set; }
        public Nullable<double> Price { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
    }
}