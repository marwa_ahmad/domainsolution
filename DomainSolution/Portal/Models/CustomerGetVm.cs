﻿
namespace Portal.Models
{
    public class CustomerGetVm
    {
         public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}