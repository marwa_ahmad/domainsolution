﻿using System.ComponentModel.DataAnnotations;

namespace Portal.Models
{
    public class CustomerCreateVm
    {
        [Required]
        public string Name { get; set; }
        
        [Required]        
        public string Email { get; set; }
    }
}