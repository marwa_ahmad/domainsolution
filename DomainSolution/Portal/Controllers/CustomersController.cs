﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL.DAL;
using Portal.Models;

namespace Portal.Controllers
{
    public class CustomersController : ApiController
    {
        // GET api/customers
        /// <summary>
        /// Returns a list of customers, without orders
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            using (var dbContext = new DomainEntities())
            {
                var customers = dbContext.tb_customer;
                if (customers.Any() == false) return Request.CreateResponse(HttpStatusCode.OK); ;
                
                var customersList = new List<CustomerGetVm>();
                foreach (var customer in customers.ToList())
                {
                    customersList.Add(new CustomerGetVm()
                    {
                        Email = customer.Email,
                        Id = customer.Id,
                        Name = customer.Name
                    });
                }
                return Request.CreateResponse(HttpStatusCode.OK, customersList);
            }
        }

        // GET api/customers/5
        /// <summary>
        /// Returns customer and his order list 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage Get(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "CustomerId is required");
            }
            using (var dbContext = new DomainEntities())
            {
                var customer = dbContext.tb_customer.Find(id);
                if (customer == null) return Request.CreateResponse(HttpStatusCode.OK); ;

                List<OrderGetVm> ordersVm;
                var customerVm = new CustomerDataGetVm()
                {
                    Email = customer.Email,
                    Id = customer.Id,
                    Name = customer.Name
                };
                var orders = customer.tb_order.ToList();
                if (orders.Any() == false) ordersVm = null;
                else
                {
                    ordersVm = orders.Select(order => new OrderGetVm()
                    {
                        Id = order.Id,
                        CreatedDate = order.CreatedDate,
                        Price = order.Price
                    }).ToList();
                }
                customerVm.Orders = ordersVm;
                return Request.CreateResponse(HttpStatusCode.OK, customerVm);
            }
        }

        // POST api/customers  
        /// <summary>
        /// to add a new customer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public HttpResponseMessage Post([FromBody]CustomerCreateVm model)
        {
            if (ModelState.IsValid == false)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invali input values.");
            }
            using (var dbContext = new DomainEntities())
            {
                dbContext.tb_customer.Add(new tb_customer()
                {
                    Email = model.Email,
                    Id = Guid.NewGuid().ToString("N"),
                    Name = model.Name
                });
                dbContext.SaveChanges();
            }
            return Request.CreateResponse(HttpStatusCode.Accepted);
        }

        // PUT api/customers/5  
        
        /// <summary>
        /// adds a new order for existing customer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public HttpResponseMessage Put(string id, [FromBody]OrderCreateVm model)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "CustomerId is required");
            }
            if (ModelState.IsValid == false)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid input values.");
            }
            using (var dbContext = new DomainEntities())
            {
                var customer = dbContext.tb_customer.Find(id);
                if (customer == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Customer not found.");                    
                }
                customer.tb_order.Add(new tb_order()
                {
                    Id = Guid.NewGuid().ToString("N"),
                    CreatedDate = DateTime.UtcNow,
                    Price = model.Price
                });
                dbContext.tb_customer.Attach(customer);
                dbContext.SaveChanges();
            }
            return Request.CreateResponse(HttpStatusCode.Accepted);
        }
    }
}
