/*DB creation*/
USE master;
GO
	IF (NOT EXISTS (
					SELECT name 
					FROM master.dbo.sysdatabases 
					WHERE name = 'Domain'))
		CREATE DATABASE Domain
GO
/*Tables creation*/
USE Domain
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

/*1. Customer*/
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tb_customer]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[tb_customer](
	[Id] [nvarchar](32) NOT NULL,
	[Name] [varchar](150) NOT NULL,
	[Email] [nvarchar](255) NULL,
	 CONSTRAINT [PK_tb_customer] PRIMARY KEY([Id] ASC))
	
	SET ANSI_PADDING OFF
END
GO
/*2. Order*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

/*3. ShoppingCart*/
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tb_order]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[tb_order](
	[Id] [nvarchar](32) NOT NULL,
	[Price] [float] NULL,
	[CreatedDate] [datetime] NULL,
	[CustomerId] [nvarchar](32) NULL,
	
	 CONSTRAINT [PK_tb_order] PRIMARY KEY([Id] ASC))	
	SET ANSI_PADDING OFF	
	
	ALTER TABLE [dbo].[tb_order]  WITH CHECK ADD  CONSTRAINT [FK_tb_order_tb_customer] FOREIGN KEY([CustomerId])
	REFERENCES [dbo].[tb_customer] ([Id])

	ALTER TABLE [dbo].[tb_order] CHECK CONSTRAINT [FK_tb_order_tb_customer]	
END
GO

